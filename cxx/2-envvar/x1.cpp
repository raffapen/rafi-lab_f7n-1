
#include <string>
#include <stdio.h>
#include <cassert>

#include "magenta/text/defs.h"

using std::string;
using magenta::text;

#if 0

class Thing
{
	int _id;
	text _val;

public:
	int id() const { return _id; }

	text &value() { return _val; }
	const text &value() const { return _val; }

public:
	Thing(int id, const char *name) : _id(id), _val(name) {}
	Thing(int id, const string &name) : _id(id), _val(name) {}

	operator text&() { return value(); }
	operator const text&() const { return value(); }

	//bool operator==(const Thing &t) const { return value() == t.value(); }
	bool operator==(const text &t) const { return value() == t; }

	const text &operator*() const { return value(); }
		
	Thing &operator=(const text &s) { value() = s; return *this; }
};

inline text operator+(const Thing &t, const text &s) { return *t + s; }
inline text operator+(const text &s, const Thing &t) { return s + *t; }

#else

#define EnvVar Thing

class EnvVar
{
	text _name;
	text _val;

	EnvVar() {}

public:
	explicit EnvVar(text name, text defval = "") : _name(name)
	{
		const char *val = getenv(+name);
		value() = val ? val : defval;
	}

	text &value() { return _val; }
	const text &value() const { return _val; }

	operator text&() { return value(); }
	operator const text&() const { return value(); }

	bool operator==(const text &t) const { return value() == t; }

	const text &operator*() const { return value(); }
//	Path operator~() const { return ~value(); }
	bool operator!() const { return !value(); }
		
	EnvVar &operator=(const text &s) { value() = s; return *this; }

	void set()
	{
//		_putenv(+stringf("%s=%s", +_name, +value()));
	}
};

inline text operator+(const EnvVar &v, const text &s) { return *v + s; }

#endif

//---------------------------------------------------------------------------------------------

void f0()
{
	const char *a = "a";
	string b("b");
	text c("c");
	Thing d(1, "d");
#define x(s) *Thing(1, #s)

	assert(a + b + c + d == x(abcd));
	assert(a + b + d + c == x(abdc));
	assert(a + c + b + d == x(acbd));
	assert(a + c + d + b == x(acdb));
	assert(a + d + b + c == x(adbc));
	assert(a + d + c + b == x(adcb));
	assert(b + a + c + d == x(bacd));
	assert(b + a + d + c == x(badc));
	assert(b + c + a + d == x(bcad));
	assert(b + c + d + a == x(bcda));
	assert(b + d + a + c == x(bdac));
	assert(b + d + c + a == x(bdca));
	assert(c + a + b + d == x(cabd));
	assert(c + a + d + b == x(cadb));
	assert(c + b + a + d == x(cbad));
	assert(c + b + d + a == x(cbda));
	assert(c + d + a + b == x(cdab));
	assert(c + d + b + a == x(cdba));
	assert(d + a + b + c == x(dabc));
	assert(d + a + c + b == x(dacb));
	assert(d + b + a + c == x(dbac));
	assert(d + b + c + a == x(dbca));
	assert(d + c + a + b == x(dcab));
	assert(d + c + b + a == x(dcba));
	
#undef x	
}

//---------------------------------------------------------------------------------------------

#if 1

void f1()
{
	text p1 = "jojo";
	text t = "foo";
	string a = p1 + " " + t + "/sys/bin/mk.pl";
	string b = p1 + string(" ");// + t + string("/sys/bin/mk.pl");
	string c = string(" ") + p1;
	string d = p1 + string(" ") + t + "/sys/bin/mk.pl";
}

void f2()
{
	Thing t(1, "jojo");
	Thing p1(1,
		t + "/a;" +
		t + "/b;" +
		t + "/c;" +
		t + "/d;");

	text s1 =
		t + "/a;" +
		t + "/b;" +
		t + "/c;" +
		t + "/d;";

	string a = p1 + " " + t + "/sys/bin/mk.pl";
	string c = string(" ") + p1;
	string c1 = " " + p1;
	string d = p1 + string(" ") + t + string("/sys/bin/mk.pl");
}

void f3()
{
	string s = "abc";
	string s1 = s + "def";
	string s2 = "def" + s;
	auto s2a = "def" + s;
	string s3 = s + "def" + s;
	string s4 = s + "def" + "ghi";
}

void f4()
{
	text t = "abc";
	text t1 = t + "def";
	text t2 = "def" + t;
	auto t2a = "def" + t;
	text t3 = t + "def" + t;
	text t4 = t + "def" + "ghi";
	text t5 = t + t1;
}

void f5()
{
	text t = "abc";
	string s = "def";
	text t1 = t + s;
	text t2 = s + t;
	auto t2a = s + t;
	text t3 = t + "def" + s + "ghi" + t;
}

void f6()
{
	Thing g(1, "123");
	text t = "abc";
	string s = "def";
	Thing g1(1, g + t);
	Thing g2(1, t + g);
	auto t2a = t + g;
	Thing g3(1, g + "def" + s + "ghi" + t);
}

#endif

//---------------------------------------------------------------------------------------------

int main()
{
	f0();
	f1();
	f2();
	f3();
	f4();
	f5();
	f6();
	return 0;
}
