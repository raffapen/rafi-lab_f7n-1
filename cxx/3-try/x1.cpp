
#define try_set(f, T) \
	try { \
		f = new T; \
	} \
	catch (exception) {}

try_set(x, Foo(abc, def));
