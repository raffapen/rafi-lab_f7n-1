
#include <iostream>

#include "magenta/text/defs.h"
#include "magenta/files/defs.h"

#include "minIni.h"

using namespace magenta;

class IniFile
{
	minIni _ini;

public:
	IniFile(const Path &file) : _ini(+file) {}

	class Section;
	class Value;
	friend Section;
	friend Value;

	class Value
	{
		Section &_section;
		text _name;
		
	public:
		friend Section;
	
		Value(Section &section, const text &name) : _section(section), _name(name) {}
		
		Value &operator=(const text &val);
		operator text() const;
		bool operator!() const { return !text(); }
	};

	class Section
	{
		IniFile &_ifile;
		text _name;

	public:
		friend Value;

		Section(IniFile &ifile, const text &name) : _ifile(ifile), _name(name) {}
		
		const text &name() const { return _name; }
		Value operator[](const text &name) { return Value(*this, name); }
	};

	Section operator[](const text &name) { return Section(*this, name); }
};

IniFile::Value::operator text() const { return _section._ifile._ini.gets(_section.name(), _name); }
IniFile::Value &IniFile::Value::operator=(const text &val) { _section._ifile._ini.put(_section.name(), _name, val); return *this; }

int main(int argc, char *argv[])
{
	IniFile ini("freemason.ini");
	text root = ini["freemason"][argv[1]];
	std::cout << "*" << root << "*" << std::endl;
	if (argc > 1)
		ini["freemason"][argv[1]] = argv[2];
	return 0;
}
