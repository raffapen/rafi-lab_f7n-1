
#include "docopt.h"

#include <iostream>

static const char USAGE[] =
R"(Naval Fate.

    Usage:
      naval_fate ship new <name>...
      naval_fate ship <name> move <x> <y> [--speed=<kn>]
      naval_fate ship shoot <x> <y>
      naval_fate mine (set|remove) <x> <y> [--moored | --drifting]
      naval_fate (-h | --help)
      naval_fate --version

    Options:
      -h --help     Show this screen.
      --version     Show version.
      --speed=<kn>  Speed in knots [default: 10].
      --moored      Moored (anchored) mine.
      --drifting    Drifting mine.
)";

static const char USAGE1[] =
R"(Freemason/mk.

Usage:
  mk [options] [--] [<make-options>...]
  mk -h | --help
  mk --version

Options:
  -d --deep      Build dependent modules
  -j --jobs=<n>  Parallel jobs (JOBS=<n>)
  -l --local     Run locally (do not distribute jobs: DIST=0)
  -x --no-log    Write log to console (LOG=0)
  -c --commands  Show executed commands (SHOW_CMD=1)
  -v --verbose   Show configuration environment variables
  --shell        Invoke shell instead of make
  -g --global    Use global setup (pointed to by FREEMASON_ROOT)
                   rather than view setup
  -h --help      Show this screen
  --version      Show version)";

int main(int argc, const char** argv)
{
    std::map<std::string, docopt::value> args = docopt::docopt(USAGE1, 
		{ argv + 1, argv + argc }, true, "Freemason 1.50", true);

    for(auto const& arg : args)
        std::cout << arg.first << ": " << arg.second << std::endl;

    return 0;
}
