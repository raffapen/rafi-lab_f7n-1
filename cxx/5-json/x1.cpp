
#include "docopt.h"

#include "json.hpp"
using Json = nlohmann::json;

#include <iostream>
#include <strstream>

static const char USAGE[] =
R"(Freemason/mk.

Usage:
  mk [options] [--] [<make-options>...]
  mk -h | --help
  mk --version

Options:
  -d --deep      Build dependent modules
  -j --jobs=<n>  Parallel jobs (JOBS=<n>)
  -l --local     Run locally (do not distribute jobs: DIST=0)
  -c --console   Write log to console (LOG=0)
  -v --verbose   Show configuration environment variables
  --shell        Invoke shell instead of make
  -g --global    Use global setup (pointed to by FREEMASON_ROOT)
                   rather than view setup
  -h --help      Show this screen
  --version      Show version)";

namespace docopt
{
void to_json(Json &j, const std::map<std::string, docopt::value> &args)
{
	for(auto const& arg : args)
	{
		if (arg.second.isStringList())
			j[arg.first] = arg.second.asStringList();
		else
		{
			std::strstream ss;
			ss << arg.second << std::ends;
			j[arg.first] = ss.str();
		}
	}
}
}

int main(int argc, const char** argv)
{
	try
	{
		std::map<std::string, docopt::value> args = docopt::docopt(USAGE, 
			{ argv + 1, argv + argc }, true, "Freemason 1.50", true);

//		for(auto const& arg : args)
//			std::cout << arg.first << ": " << arg.second << std::endl;

		Json j = {
			{"pi", 3.141},
			{"happy", true},
			{"name", "Niels"},
			{"nothing", nullptr},
			{"answer", {
				{"everything", 42}
			}},
			{"list", {1, 0, 2}},
			{"object", {
				{"currency", "USD"},
				{"value", 42.99}
			}}
		};
	
	//	std::cout << std::setw(4) << j << std::endl;
	
#if 0
		Json j1;
		for(auto const& arg : args)
		{
			if (arg.second.isStringList())
				j1[arg.first] = arg.second.asStringList();
			else
			{
				std::strstream ss;
				ss << arg.second << std::ends;
				j1[arg.first] = ss.str();
			}
		}
#endif
		Json j2(args);
		std::cout << std::setw(4) << j2 << std::endl;
	}
	catch (std::exception &x)
	{
		std::cout << "error: " << x.what() << std::endl;
	}
	
    return 0;
}
