
#include "magenta/text/defs.h"

#include <iostream>

using namespace magenta;
using namespace std;

static const char USAGE[] =
R"(Freemason/mk.

Usage:
  mk [options] [--] [<make-options>...]
  mk -h | --help
  mk --version

Options:
  -d --deep      Build dependent modules
  -j --jobs=<n>  Parallel jobs (JOBS=<n>)
  -l --local     Run locally (do not distribute jobs: DIST=0)
  -c --console   Write log to console (LOG=0)
  -v --verbose   Show configuration environment variables
  --shell        Invoke shell instead of make
  -g --global    Use global setup (pointed to by FREEMASON_ROOT)
                   rather than view setup
  -h --help      Show this screen
  --version      Show version)";

int main(int argc, const char** argv)
{
	try
	{
		map<string, docopt::value> args = docopt::docopt(USAGE, 
			{ argv + 1, argv + argc }, true, "Freemason 1.50", true);

		JSON j = {
			{"pi", 3.141},
			{"happy", true},
			{"name", "Niels"},
			{"nothing", nullptr},
			{"answer", {
				{"everything", 42}
			}},
			{"list", {1, 0, 2}},
			{"object", {
				{"currency", "USD"},
				{"value", 42.99}
			}}
		};
	
		JSON j2(args);
		cout << std::setw(4) << j2 << endl;
	}
	catch (std::exception &x)
	{
		cout << "error: " << x.what() << endl;
	}
	
    return 0;
}
