
#include <iostream>
#include <string>
#include <any>

#include "magenta/text/defs.h"

#include "yaml-cpp/yaml.h"

using namespace std;
using namespace magenta;

//#define F_TEXT_JSON
#define F_TEXT_JSON1
// #define F_DOCOPT_TO_JSON
#define F_YAML
#define F_YAML_TO_JSON

///////////////////////////////////////////////////////////////////////////////////////////////

static const char USAGE[] =
R"(Freemason/mk.

Usage:
  mk [options] [--] [<make-options>...]
  mk -h | --help
  mk --version

Options:
  -s --spec=<n>  Build SPEC number <n> from SPECS list
  -d --deep      Build dependent modules
  -j --jobs=<n>  Parallel jobs (JOBS=<n>)
  -l --local     Run locally (do not distribute jobs: DIST=0)
  -c --console   Write log to console (LOG=0)
  -v --verbose   Show configuration environment variables
  --shell        Invoke shell instead of make
  -g --global    Use global setup (pointed to by FREEMASON_ROOT)
                   rather than view setup
  --show-specs   Print SPECS list
  --show-deps    Print dependency tree
  -h --help      Show this screen
  --version      Show version)";

//---------------------------------------------------------------------------------------------

std::any ston(const string &s)
{
	char *p;
	long n = strtol(s.c_str(), &p, 10);
	if (!!p && p - s.c_str() == s.length())
		return n;
	double d;
	if (sscanf(s.c_str(), "%lf", &d) == 1)
		return d;
	return std::any();
}

//---------------------------------------------------------------------------------------------

string to_lower(const string &s)
{
	static locale loc;
	string s1;
	for (auto c : s)
    	s1 += std::tolower(c, loc);
   	return s1;
}

//---------------------------------------------------------------------------------------------

#ifdef F_TEXT_JSON
JSON to_text_json(const JSON &j)
{
	JSON j1;
	for (auto it = j.begin(); it != j.end(); ++it)
	{
		auto k = it.key();
		auto v = it.value();

		cout << "k=" << k << endl;
		cout << "v=" << v << endl;

		if (it->is_object())
		{
			j1[k] = to_text_json(v);
		}
		else if (it->is_array())
		{
			for (auto &e : v)
				j1[k].push_back(to_text_json(e));
		}
		else if (it->is_string())
		{
			j1[k] = v.get<string>();
		}
		else if (it->is_null())
		{
			j1[k] = "";
		}
		else
		{
			strstream ss;
			ss << v << std::ends;
			j1[k] = ss.str();
		}
		cout << "j1=" << j1 << endl;
	}
	return j1;
}
#endif

//---------------------------------------------------------------------------------------------

#ifdef F_TEXT_JSON1
JSON to_text_json1(const JSON &j)
{
	JSON j1;
	if (j.is_object())
	{
		for (auto it = j.begin(); it != j.end(); ++it)
		{
			auto k = it.key();
			auto v = it.value();
			j1[k] = to_text_json1(v);
		}
	}
	else if (j.is_array())
	{
		for (auto &e : j)
			j1.push_back(to_text_json1(e));
	}
//	else if (j.is_string())
//	{
//		j1 = j.get<string>();
//	}
	else if (j.is_number() ||  j.is_boolean() || j.is_string() || j.is_null())
	{
		j1 = j;
	}
//	else if (j.is_null())
//	{
//		j1 = "";
//	}
	else
	{
		strstream ss;
		ss << j << std::ends;
		j1 = ss.str();
	}

	return j1;
}
#endif

//---------------------------------------------------------------------------------------------

#ifdef F_YAML
YAML::Node to_yaml(const JSON &j)
{
	YAML::Node y;
	if (j.is_object())
	{
		for (auto it = j.begin(); it != j.end(); ++it)
		{
			auto k = it.key();
			auto v = it.value();
			y[k] = to_yaml(v);
		}
	}
	else if (j.is_array())
	{
		for (auto &e : j)
			y.push_back(to_yaml(e));
	}
	else if (j.is_string())
	{
		y = j.get<string>();
	}
//	else if (j.is_number())
//	{
//		y = j;
//	}
	else if (j.is_boolean())
	{
		y = j.get<bool>() ? "yes" : "no";
	}
	else if (j.is_null())
	{
		y = "";
	}
	else
	{
		strstream ss;
		ss << j << std::ends;
		y = ss.str();
	}
	return y;
}
#endif

//---------------------------------------------------------------------------------------------

#ifdef F_YAML_TO_JSON
JSON to_json3(const YAML::Node &y)
{
	JSON j;
	if (y.IsMap())
	{
		//for (YAML::const_iterator it = y.begin(); it != y.end(); ++it)
		for (auto it = y.begin(); it != y.end(); ++it)
		{
			auto k = it->first.as<string>();
			auto v = it->second;
			j[k] = to_json3(v);
		}
	}
	else if (y.IsSequence())
	{
		for (auto &e : y)
			j.push_back(to_json3(e));
	}
	else if (y.IsScalar())
	{
		auto s = y.as<string>();
#if 1
		auto n = ston(s);
		if (n.has_value())
		{
			if (n.type() == typeid(long))
				j = any_cast<long>(n);
			else if (n.type() == typeid(double))
				j = any_cast<double>(n);
			else
				j = s;
		}
		else
		{
			j = s;
		}
#else
		double d;
		long n;
		int xd, xn;
		xd = sscanf(s.c_str(), "%lf", &d);
		char *p;
		long n1 = strtol(s.c_str(), &p, 10);
		bool isn = !!p && p - s.c_str() == s.length();
		xn = sscanf(s.c_str(), "%ld", &n);
		if (!xd && !xn)
		{
			auto s1 = to_lower(s);
			if (s == "true" || s == "yes")
				j = true;
			else if (s == "false" || s == "no")
				j = false;
			else // string
			{
				j = s;
			}
		}
		else if (isn)
			j = n;
		else
			j = d;
#endif

#if 0
		if ((bool) std::from_chars(s.begin(), s.end(), d).ec == false) // float
		{
			j = d;
		}
		if ((bool) std::from_chars(s.begin(), s.end(), n).ec == false) // integer
		{
			j = n;
		}
		else /* boolean or string*/
		{
			auto s1 = tolower(s, loc);
			if (s == "true" || s == "yes")
				j = true;
			else if (s == "false" || s == "no")
				j = false;
			else // string
			{
				j = s;
			}
		}
#endif
	}
//	else if (j.is_number())
//	{
//		y = j;
//	}
//	else if (j.is_boolean())
//	{
//		y = j.get<bool>() ? "yes" : "no";
//	}
	else if (y.IsNull())
	{
		j = nullptr;
	}
	else
	{
		strstream ss;
		ss << y << std::ends;
		j = ss.str();
	}

	return j;
}
#endif

//---------------------------------------------------------------------------------------------

int main(int argc, const char** argv)
{
	try
	{
		//map<string, docopt::value> args = docopt::docopt(USAGE, 
		//	{ argv + 1, argv + argc }, true, "Freemason 1.50", true);

		JSON ja = {
			{"object", {
				{"currency", "USD"},
				{"value", 42.99}
			}}
		};
		JSON j = {
			{"pi", 3.141},
			{"happy", true},
			{"name", "Niels"},
			{"nothing", nullptr},
			{"answer", {
				{"everything", 42}
			}},
			{"numbers", {1, 0, 2}},
			{"strings", {"t1", "t0", "t2"}},
			{"array", {"t1", 0, "t2"}},
			{"nested-array", {"t1", 0, {"everything", 42}}},
			{"array-with-obj", {"t1", 0, ja}},
			{"object", {
				{"currency", "USD"},
				{"value", 42.99}
			}}
		};

		cout << "type: " << j.type_name() << endl;
		cout << std::setw(4) << j << endl;

#ifdef F_DOCOPT_TO_JSON
		cout << endl << endl;
		JSON j2(args);
		cout << j2.dump(4) << endl;
#endif

#ifdef F_TEXT_JSON1
		cout << endl << endl;

		auto j1 = to_text_json1(j);
		cout << j1.dump(4) << endl;
#endif

#ifdef F_YAML
		cout << endl << endl;
		
		auto y = to_yaml(j);
		cout << std::setw(4) << y << endl;
#endif

#if defined(F_YAML) && defined(F_YAML_TO_JSON)
		cout << endl << endl;

		auto j2 = to_json3(y);
		cout << j2.dump(4) << endl;
#endif
	}
	catch (std::exception &x)
	{
		cout << "error: " << x.what() << endl;
	}
	catch (...)
	{
		cout << "unknown error" << endl;
	}
	
    return 0;
}

///////////////////////////////////////////////////////////////////////////////////////////////
